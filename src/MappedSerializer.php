<?php

declare(strict_types=1);

namespace NigDevteam;

use LogicException;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\MessageDecodingFailedException;
use Symfony\Component\Messenger\Stamp\NonSendableStampInterface;
use Symfony\Component\Messenger\Stamp\SerializerStamp;
use Symfony\Component\Messenger\Stamp\StampInterface;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface as MessengerSerializer;
use Symfony\Component\Serializer\SerializerInterface;

class MappedSerializer implements MessengerSerializer
{
    private SerializerInterface $serializer;
    /** @var array<class-string, string> */
    private array $encodeMapping;
    /** @var array<string, class-string> */
    private array $decodeMapping;

    private ?string $fallbackDecodeEvent;

    /**
     * @param array<class-string, string> $encodeMapping
     * @param array<string, class-string> $decodeMapping
     */
    public function __construct(
        SerializerInterface $serializer,
        array $encodeMapping,
        array $decodeMapping,
        ?string $fallbackDecodeEvent = null
    ) {
        $this->serializer = $serializer;
        $this->encodeMapping = $encodeMapping;
        $this->decodeMapping = $decodeMapping;
        $this->fallbackDecodeEvent = $fallbackDecodeEvent;
    }

    /**
     * @phpstan-ignore-next-line T is infered within the function
     * @template T of object
     * @phpstan-ignore-next-line contravariance is respected here, parent class does not respect phpstan
     * @param array{body: string, headers?: array{stamps?: string}} $encodedEnvelope
     * @return Envelope
     * @throws MessageDecodingFailedException
     */
    public function decode(array $encodedEnvelope): Envelope
    {
        $body = $encodedEnvelope['body'];
        $headers = $encodedEnvelope['headers'] ?? [];

        if (!isset($headers['stamps'])) {
            if ($this->fallbackDecodeEvent !== null) {
                return $this->unserializeAs(
                    $this->getClass($this->fallbackDecodeEvent),
                    $body,
                    []
                );
            }
            throw new LogicException("No stamps to decode");
        }

        $stampsData = $headers['stamps'];
        /**
         * @var array<StampInterface|array<StampInterface>> $rawStamps
         *
         * <p>
         * allowed_classes => true means we accept all possible classes.
         * It <i>is</i> risky, but still better than putting all classes ?
         * </p>
         */
        $rawStamps = unserialize($stampsData, ['allowed_classes' => true]);
        $stamps = self::flatten($rawStamps);

        $eventName = $this->extractEventName(
            array_filter(
                $stamps,
                static fn(StampInterface $stamp): bool => $stamp instanceof SerializerStamp
            )
        );

        if ($eventName === null) {
            $eventName = $this->fallbackDecodeEvent;
        }

        if ($eventName === null) {
            throw new LogicException("No eventName provided in stamps");
        }

        if (!array_key_exists($eventName, $this->decodeMapping)) {
            throw new LogicException("$eventName not in decode list");
        }

        return $this->unserializeAs($this->getClass($eventName), $body, $stamps);
    }

    /**
     * @template T of object
     * @param class-string<T> $class
     * @param StampInterface[] $stamps
     */
    private function unserializeAs(string $class, string $body, array $stamps): Envelope
    {
        /**
         * @var T $message
         */
        $message = $this->serializer->deserialize($body, $class, 'json');
        return new Envelope($message, $stamps);
    }

    /**
     * @param Envelope $envelope
     * @return array{body: string, headers:array{stamps: string}}
     */
    public function encode(Envelope $envelope): array
    {
        $message = $envelope->getMessage();

        $allStamps = [];
        foreach ($envelope->withoutStampsOfType(NonSendableStampInterface::class)->all() as $stamp) {
            $allStamps[] = $stamp;
        }

        /** @var array<SerializerStamp> $stamps */
        $stamps = $envelope->all(SerializerStamp::class);
        $eventName = $this->extractEventName($stamps);
        if ($eventName === null) {
            $messageClass = get_class($message);
            if (!array_key_exists($messageClass, $this->encodeMapping)) {
                throw $this->makeException(
                    $messageClass,
                    "encodeMapping",
                    "decodeMapping",
                    array_flip($this->decodeMapping)
                );
            }
            $allStamps[] = new SerializerStamp(['eventName' => $this->encodeMapping[$messageClass]]);
        }

        return [
            'body' => $this->serializer->serialize($message, 'json'),
            'headers' => [
                'stamps' => serialize($allStamps)
            ],
        ];
    }

    /**
     * @param array<SerializerStamp> $stamps
     * @return string|null
     */
    protected function extractEventName(array $stamps): ?string
    {
        foreach ($stamps as $stamp) {
            if (array_key_exists('eventName', $stamp->getContext())) {
                return $stamp->getContext()['eventName'];
            }
        }
        return null;
    }

    /**
     * @param array<StampInterface|array<StampInterface>> $stamps
     * @return array<int, StampInterface>
     */
    private static function flatten(array $stamps): array
    {
        $flattended = [];
        array_walk_recursive($stamps, static function (StampInterface $stamp) use (&$flattended): void {
            $flattended[] = $stamp;
        });
        return $flattended;
    }

    /**
     * @param string $eventName
     * @return class-string
     */
    protected function getClass(string $eventName): string
    {
        if (array_key_exists($eventName, $this->decodeMapping)) {
            return $this->decodeMapping[$eventName];
        }
        throw $this->makeException($eventName, "decodeMapping", "encodeMapping", array_flip($this->encodeMapping));
    }

    /**
     * @param array<string, string> $flipped
     * @return LogicException
     */
    protected function makeException(string $symbol, string $source, string $cible, array $flipped): LogicException
    {
        return new LogicException(
            sprintf(
                "Impossible to extract mapping for \%s because it is not in $source (%s).\n"
                . "Try adding the missing mapping to the $source field in the configuration",
                $symbol,
                array_key_exists($symbol, $flipped)
                    ? "It is in $cible"
                    : "It is not in $cible either"
            )
        );
    }
}
