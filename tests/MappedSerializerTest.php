<?php

declare(strict_types=1);

namespace Tests;

use NigDevteam\MappedSerializer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Messenger\Envelope;

class MappedSerializerTest extends TestCase
{
    public function testFullCircle(): void
    {
        $serializer = new MappedSerializer(new ForwardingSerializer(), [
            From::class => 'newElementEncoded'
        ], [
            'newElementEncoded' => To::class
        ]);

        $serializedEnvelope = $serializer->encode(
            (new Envelope(new From("12345"), [new CustomStamp1()]))
                ->with(new CustomStamp2())
        );

        $previousObject = $serializer->decode($serializedEnvelope)->getMessage();

        self::assertInstanceOf(To::class, $previousObject);

        self::assertSame('12345', $previousObject->secret);
    }

    public function testPartialDecode(): void
    {
        $serialized = ["body" => json_encode(["secret" => "12345"], JSON_THROW_ON_ERROR)];

        $serializer = new MappedSerializer(new ForwardingSerializer(), [], ['event' => To::class], 'event');

        $previousObject = $serializer->decode($serialized)->getMessage();

        self::assertInstanceOf(To::class, $previousObject);

        self::assertSame('12345', $previousObject->secret);
    }

    public function testError(): void
    {
        $serializer = new MappedSerializer(new ForwardingSerializer(), [
            From::class => 'newElementEncoded'
        ], [
            'newElementEncoded' => To::class
        ]);

        $this->expectException(\LogicException::class);
        $this->expectExceptionMessage(
            "Impossible to extract mapping for \Tests\To "
            . "because it is not in encodeMapping (It is in decodeMapping).\n"
            . "Try adding the missing mapping to the encodeMapping field in the configuration"
        );
        $serializer->encode(
            (new Envelope(new To("12345"), [new CustomStamp1()]))
                ->with(new CustomStamp2())
        );
    }

    public function testNonSendableStampIsNotSent(): void
    {
        // Given
        $serializer = new MappedSerializer(new ForwardingSerializer(), [
            From::class => 'newElementEncoded'
        ], [
            'newElementEncoded' => To::class
        ]);

        // When
        $serializedEnvelope = $serializer->encode(
            (new Envelope(new From("12345"), [new CustomNonSendableStamp()]))
        );

        $decodedEnvelop = $serializer->decode($serializedEnvelope);

        // Then
        $previousObject = $decodedEnvelop->getMessage();

        self::assertInstanceOf(To::class, $previousObject);

        self::assertSame('12345', $previousObject->secret);

        self::assertStringNotContainsString(
            'CustomNonSendableStamp',
            print_r(
                $serializedEnvelope['headers'],
                true
            )
        );
    }
}
