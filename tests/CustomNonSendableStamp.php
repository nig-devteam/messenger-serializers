<?php

declare(strict_types=1);

namespace Tests;

use Symfony\Component\Messenger\Stamp\NonSendableStampInterface;

class CustomNonSendableStamp implements NonSendableStampInterface
{
}
