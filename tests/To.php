<?php

declare(strict_types=1);

namespace Tests;

class To
{
    public string $secret;

    public function __construct(string $secret)
    {
        $this->secret = $secret;
    }
}
