<?php

declare(strict_types=1);

namespace Tests;

use Symfony\Component\Serializer\SerializerInterface;

class ForwardingSerializer implements SerializerInterface
{
    /**
     * @param string $format
     */
    public function serialize($data, $format, array $context = []): string
    {
        return json_encode($data, JSON_THROW_ON_ERROR);
    }

    /**
     * @param string $type
     * @param string $format
     */
    public function deserialize($data, $type, $format, array $context = [])
    {
        $stdClass = json_decode($data, false, 512, JSON_THROW_ON_ERROR);
        if ($type === To::class) {
            return new To($stdClass->secret);
        }
        return null;
    }
}
